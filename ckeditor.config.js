/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

function debug(txt, sep) {
    if (sep === true)
        console
            .log('=========================================================================================================================================');
    console.log(txt);
}

var getUrl = window.location;
var baseUrl = getUrl.protocol + "//" + getUrl.host + '/sites/all/themes/rsc2014_cl/ckeditor/';

CKEDITOR.plugins.addExternal( 'autogrow', baseUrl + 'autogrow.js' );

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';

	debug(config, true);

	debug("BASE: " + baseUrl, false);
	config.width = 'auto';
	config.contentsCss = [baseUrl + 'cl-articles.css'];
	config.stylesSet = 'drupal:' + baseUrl + 'cl-articles.js';
	config.extraAllowedContent = 'p br span div table tr td th style{*}[*] img';
	config.fontSize_sizes = '2/2px;4/4px;6/6px;8/8px;9/9px;10/10px;11/11px;12/12px;14/14px;16/16px;18/18px;20/20px;22/22px;24/24px;26/26px;28/28px;36/36px;48/48px;72/72px';
	
	config.pasteFromWordCleanupFile = baseUrl + 'pastefromword-custom-filter.js';
	
	config.extraPlugins = 'autogrow';
	config.autoGrow_minHeight = 300;
	config.autoGrow_maxHeight = 500;
	config.autoGrow_onStartup = true;

	debug('CKEditor Configured!');
};
