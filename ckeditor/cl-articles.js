/**
 * Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

// This file contains style definitions that can be used by CKEditor plugins.
//
// The most common use for it is the "stylescombo" plugin, which shows a combo
// in the editor toolbar, containing all styles. Other plugins instead, like
// the div plugin, use a subset of the styles on their feature.
//
// If you don't have plugins that depend on this file, you can simply ignore it.
// Otherwise it is strongly recommended to customize this file to match your
// website requirements and design properly.

CKEDITOR.stylesSet.add( 'drupal', [
	// Block Styles */

	// These styles are already available in the "Format" combo ("format" plugin),
	// so they are not needed here by default. You may enable them to avoid
	// placing the "Format" combo in the toolbar, maintaining the same features.
	/*
	{ name: 'Paragraph',		element: 'p' },
	{ name: 'Heading 1',		element: 'h1' },
	{ name: 'Heading 2',		element: 'h2' },
	{ name: 'Heading 3',		element: 'h3' },
	{ name: 'Heading 4',		element: 'h4' },
	{ name: 'Heading 5',		element: 'h5' },
	{ name: 'Heading 6',		element: 'h6' },
	{ name: 'Preformatted Text',element: 'pre' },
	{ name: 'Address',			element: 'address' },
	*/

	{ name: '00  CL Spacer', element: 'p', attributes: { 'class': 'cls_00_Spacer' } },
	{ name: '01a CL Title', element: 'div', attributes: { 'class': 'cls_01aCLTitle' } },
	{ name: '01b CL Title with subtitle', element: 'div', attributes: { 'class': 'cls_01bCLTitlewithsubtitle' } },
    { name: '02  CL Sub Title', element: 'div', attributes: { 'class': 'cls_02CLSubTitle' } },
	{ name: '03a CL Scripture Verse', element: 'span', attributes: { 'class': 'cls_03aCLScriptureverse' } },
	{ name: '03b CL Scripture Reference', element: 'span', attributes: { 'class': 'cls_03bCLScripturereference' } },
    { name: '04  CL Small Heading 1', element: 'div', attributes: { 'class': 'cls_04CLSmallheading1' } },
    { name: '05  CL Smaller Heading 1.1', element: 'div', attributes: { 'class': 'cls_05CLSmallerheading11' } },
    { name: '06  CL Smallest Heading 1.1.1', element: 'div', attributes: { 'class': 'cls_06CLStillsmallerheading111' } },
	{ name: '07  CL Bullets', element: 'span', attributes: { 'class': 'cls_07CLBullets' } },
	{ name: '08  CL Bullets Numbering', element: 'span', attributes: { 'class': 'cls_08CLBulletsNumbering' } },
    { name: '09  CL Author Name', element: 'span', attributes: { 'class': 'cls_09CLAuthorname' } },
    { name: '10  CL Copyright Text', element: 'span', attributes: { 'class': 'cls_10CLCopyrightText' } },
    { name: '11  CL Endnotes Heading', element: 'span', attributes: { 'class': 'cls_11CLEndnotesheading' } },
    { name: '12  CL Endnotes', element: 'span', attributes: { 'class': 'cls_12CLEndnotes' } },
    { name: '13  CL Quote - Indent', element: 'span', attributes: { 'class': 'cls_13CLQuoteIndent' } },
    { name: '14  CL Quote Reference', element: 'div', attributes: { 'class': 'cls_14CLQuoteReference' } },
	{ name: '15  CL Normal Paragraph', element: 'span', attributes: { 'class': 'cls_15CLNormalParagraph' } },
	{ name: '16  CL Ref No', element: 'span', attributes: { 'class': 'cls_RefNo' } },
	{ name: '17  CL Reference', element: 'a', attributes: { 'class': 'cls_Ref' } }

	// Inline Styles */

	// These are core styles available as toolbar buttons. You may opt enabling
	// some of them in the Styles combo, removing them from the toolbar.
	// (This requires the "stylescombo" plugin)
	/*
	{ name: 'Strong',			element: 'strong', overrides: 'b' },
	{ name: 'Emphasis',			element: 'em'	, overrides: 'i' },
	{ name: 'Underline',		element: 'u' },
	{ name: 'Strikethrough',	element: 'strike' },
	{ name: 'Subscript',		element: 'sub' },
	{ name: 'Superscript',		element: 'sup' },
	*/

    /*
	{ name: 'Big',				element: 'big' },
	{ name: 'Small',			element: 'small' },
	{ name: 'Typewriter',		element: 'tt' },

	{ name: 'Computer Code',	element: 'code' },
	{ name: 'Keyboard Phrase',	element: 'kbd' },
	{ name: 'Sample Text',		element: 'samp' },
	{ name: 'Variable',			element: 'var' },

	{ name: 'Deleted Text',		element: 'del' },
	{ name: 'Inserted Text',	element: 'ins' },

	{ name: 'Cited Work',		element: 'cite' },
	{ name: 'Inline Quotation',	element: 'q' },

	{ name: 'Language: RTL',	element: 'span', attributes: { 'dir': 'rtl' } },
	{ name: 'Language: LTR',	element: 'span', attributes: { 'dir': 'ltr' } },
    */
	/* Object Styles */

    /*
	{
		name: 'Styled image (left)',
		element: 'img',
		attributes: { 'class': 'left' }
	},

	{
		name: 'Styled image (right)',
		element: 'img',
		attributes: { 'class': 'right' }
	},

	{
		name: 'Compact table',
		element: 'table',
		attributes: {
			cellpadding: '5',
			cellspacing: '0',
			border: '1',
			bordercolor: '#ccc'
		},
		styles: {
			'border-collapse': 'collapse'
		}
	},

	{ name: 'Borderless Table',		element: 'table',	styles: { 'border-style': 'hidden', 'background-color': '#E6E6FA' } },
	{ name: 'Square Bulleted List',	element: 'ul',		styles: { 'list-style-type': 'square' } }
    */
] );

